<link href="bg.css" type="text/css" rel="stylesheet">
<?php include('includes/header.php'); ?>




<?php


    function validate($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(isset($_POST['submit'])){
// print_r($_POST);
// die();

        $name = validate($_POST['name']);
        $email = validate($_POST['email']);
        $password = validate($_POST['password']);
		$country= validate($_POST['country']);
		$contact = validate($_POST['contact']);
        $passwordconfirm = $_POST['passwordconfirm'];

        $err = 0;
        $nameErr ="";
        $emailErr="";
        $passErr="";
        $passConErr="";
		$contactErr="";
		$countryErr="";
        $cpErr="";
	


        


        if (empty($name)){
            $nameErr ="Name Field is Missing";
            $err = 1;
        }
        // else{
        //     $err=0;
        // }
        
        if (empty($email)){
            $emailErr ="Email Field is Missing";
            $err = 1;
        }
        // else{
        //     $err=0;
        // }
       
        if (empty($password)){
            $passErr ="Password Field is Missing";
            $err = 1;
        }
        // else{
        //     $err=0;
        // }
		
		if (empty($contact)){
            $contactErr ="Contact Field is Missing";
            $err = 1;
        }
        // else{
        //     $err=0;
        // }
		
		if (empty($country)){
            $countryErr ="Address Field is Missing";
            $err = 1;
        }
        // else{
        //     $err=0;
        // }
		

        if (empty($passwordconfirm)){
            $cpErr ="Password must be same";
            $err = 1;
        }
    
        if(strlen($password)<8){
            $passErr ="Password must be minimum of 8 characters";
            $err = 1;

        }
        // else{
        //     $err = 0;
        // }
        

        if($password != $passwordconfirm){
            $passConErr = "Password must be same";
            $err = 1;
        }
        // else{
        //     $err = 0;
        // }
		  
        if($err == 0 ){


            //Sessions =volatile, temporary data in the web browsers
            //cookies = non volatile

            session_start();
            $_SESSION['name']=$name;
            $_SESSION['email']=$email;
            $_SESSION['password']=$password;
			$_SESSION['country']=$country;
			$_SESSION['contact']=$contact;
			$_SESSION['bday']=$bday;
            $_SESSION['gender']=$gender;
            $_SESSION['passwordconfirm']=$passwordconfirm;   

            header("Location: dashboard.php");
        }



       
    }
?>





  
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>



<div class="row">
    <div class ="col-md-4">  </div>
    <div class ="col-md-4"> 
    <font color="white">
        <h1> LOG IN FORM </h1>

        <form>
            <div class="form-group">
                <label>Email address</label>
                <input type="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-info">Submit</button>
             <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Register</button> 
            <!-- <a href="registration.php" class="btn btn-success" >Register</button></a> -->
        </form>
    </div>
    <div class ="col-md-4">  </div>
</div>
</font>
<!-- modal start REGISTRATION FORM -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1><font color="black">Registration Form</h1>
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
            <div class="form-group">
                <label>Name</label>
                <input name="name" type="text" class="form-control" placeholder="Name">
                <?php if(isset($nameErr)){
                    echo "<div class='aler alert-danger'>$nameErr</div>";
                }
                ?>
                
            </div>
            <div class="form-group">
                <label>Email Address</label>
                <input name="email" type="email" class="form-control" placeholder="Username">
                <?php if(isset($emailErr)){
                    echo "<div class='aler alert-danger'>$emailErr</div>";
                }
                ?>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input name="password" type="password" class="form-control" placeholder="Password">
                <?php if(isset($passErr)){
                    echo "<div class='aler alert-danger'>$passErr</div>";
                }
                ?>
            </div>

            <div class="form-group">
                <label>Confirm Password</label>
                <input name="passwordconfirm" type="password" class="form-control" placeholder="Confrim password">
                <?php if(isset($passErr)){
                    echo "<div class='aler alert-danger'>$passConErr</div>";
                }

                if(isset($cpErr)){
                    echo "<div class='aler alert-danger'>$cpErr</div>";
                }
               
                ?>
            </div>
			
			
			
			<div class="form-group">
                <label>Contact Number</label>
                <input name="contact" type="text" class="form-control" placeholder="+63">
                <?php if(isset($contactErr)){
                    echo "<div class='aler alert-danger'>$contactErr</div>";
                }

                
                ?>
            </div>
			
			<div class="form-group">
                <label>Address</label>
                <input name="country" type="text" class="form-control" placeholder="Address">
                <?php if(isset($countryErr)){
                    echo "<div class='aler alert-danger'>$countryErr</div>";
                }
                ?>
            </div>
			
			
            <input type="submit" name="submit" class="btn btn-success" value="Submit">
        </form> 
        <br>

        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>

  </div>
</div>



<!-- modal end -->
<?php include('includes/footer.php');?>

   